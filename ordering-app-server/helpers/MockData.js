module.exports = [
  {
    id: 1,
    name: 'iPhone 6',
    price: 35000,
    rating: 5,
    details: 'Lorem ipsum dolor sit amet, pri summo epicuri ad, suas semper epicurei eum cu. Vel illud verterem disputationi no, est omittam intellegam et. Solum blandit accommodare pri in. Eos postea labore an, amet dictas mnesarchum an vis. Ut nec habeo labore invenire, sit accumsan salutandi at, et qui facer mazim persequeris.',
    images: [
      {
        img: 'http://store.storeimages.cdn-apple.com/8748/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphone6/plus/iphone6-plus-box-space-gray-2014?wid=478&hei=595&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=dsjlp1'
      },
      {
        img: 'http://cdn2.gsmarena.com/vv/pics/apple/apple-iphone-6-2.jpg'
      },
      {
        img: 'http://cdn.macrumors.com/article-new/2014/09/autopay.jpg'
      }
    ]
  },
  {
    id: 2,
    name: 'iPad Pro',
    price: 50000,
    rating: 3,
    details: 'Lorem ipsum dolor sit amet, pri summo epicuri ad, suas semper epicurei eum cu. Vel illud verterem disputationi no, est omittam intellegam et. Solum blandit accommodare pri in. Eos postea labore an, amet dictas mnesarchum an vis. Ut nec habeo labore invenire, sit accumsan salutandi at, et qui facer mazim persequeris.',
    images: [
      {
        img: 'http://www.bhphotovideo.com/images/images2500x2500/apple_128gb_ipad_pro_wi_fi_1185490.jpg'
      },
      {
        img: 'http://platformer.cdn.appadvice.com/wp-content/appadvice-v2-media/2016/03/ipadpro-2-_2da09dd0800a0b4f4095c6139ec5c5e4.png'
      },
      {
        img: 'https://9to5mac.files.wordpress.com/2015/11/procreate-3-ipad-pro.png'
      }
    ]
  },
  {
    id: 3,
    name: 'Macbook Pro',
    price: 55000,
    rating: 4,
    details: 'Lorem ipsum dolor sit amet, pri summo epicuri ad, suas semper epicurei eum cu. Vel illud verterem disputationi no, est omittam intellegam et. Solum blandit accommodare pri in. Eos postea labore an, amet dictas mnesarchum an vis. Ut nec habeo labore invenire, sit accumsan salutandi at, et qui facer mazim persequeris.',
    images: [
      {
        img: 'http://cdn.macrumors.com/images-new/buyers-products/rmbp-2015.png'
      },
      {
        img: 'http://images.christianpost.com/full/88311/macbook.png'
      },
      {
        img: 'http://downloadpsd.com/wp-content/uploads/Glossy-Macbook-Pro-Retina-Mockup-PSD.png'
      }
    ]
  },
  {
    id: 4,
    name: 'Macbook Pro w/ Retina Display',
    price: 85000,
    rating: 4,
    details: 'Lorem ipsum dolor sit amet, pri summo epicuri ad, suas semper epicurei eum cu. Vel illud verterem disputationi no, est omittam intellegam et. Solum blandit accommodare pri in. Eos postea labore an, amet dictas mnesarchum an vis. Ut nec habeo labore invenire, sit accumsan salutandi at, et qui facer mazim persequeris.',
    images: [
      {
        img: 'http://cdn.macrumors.com/images-new/buyers-products/rmbp-2015.png'
      },
      {
        img: 'http://images.christianpost.com/full/88311/macbook.png'
      },
      {
        img: 'http://downloadpsd.com/wp-content/uploads/Glossy-Macbook-Pro-Retina-Mockup-PSD.png'
      }
    ]
  },
  {
    id: 5,
    name: 'iPhone 6s 16gb',
    price: 40000,
    rating: 5,
    details: 'Lorem ipsum dolor sit amet, pri summo epicuri ad, suas semper epicurei eum cu. Vel illud verterem disputationi no, est omittam intellegam et. Solum blandit accommodare pri in. Eos postea labore an, amet dictas mnesarchum an vis. Ut nec habeo labore invenire, sit accumsan salutandi at, et qui facer mazim persequeris.',
    images: [
      {
        img: 'http://store.storeimages.cdn-apple.com/8748/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphone6/plus/iphone6-plus-box-space-gray-2014?wid=478&hei=595&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=dsjlp1'
      },
      {
        img: 'http://cdn2.gsmarena.com/vv/pics/apple/apple-iphone-6-2.jpg'
      },
      {
        img: 'http://cdn.macrumors.com/article-new/2014/09/autopay.jpg'
      }
    ]
  },
  {
    id: 6,
    name: 'iPhone 6s 64gb',
    price: 45000,
    rating: 5,
    details: 'Lorem ipsum dolor sit amet, pri summo epicuri ad, suas semper epicurei eum cu. Vel illud verterem disputationi no, est omittam intellegam et. Solum blandit accommodare pri in. Eos postea labore an, amet dictas mnesarchum an vis. Ut nec habeo labore invenire, sit accumsan salutandi at, et qui facer mazim persequeris.',
    images: [
      {
        img: 'http://store.storeimages.cdn-apple.com/8748/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphone6/plus/iphone6-plus-box-space-gray-2014?wid=478&hei=595&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=dsjlp1'
      },
      {
        img: 'http://cdn2.gsmarena.com/vv/pics/apple/apple-iphone-6-2.jpg'
      },
      {
        img: 'http://cdn.macrumors.com/article-new/2014/09/autopay.jpg'
      }
    ]
  },
  {
    id: 7,
    name: 'Galaxy A7 (2016)',
    price: 20000,
    rating: 3,
    details: 'Lorem ipsum dolor sit amet, pri summo epicuri ad, suas semper epicurei eum cu. Vel illud verterem disputationi no, est omittam intellegam et. Solum blandit accommodare pri in. Eos postea labore an, amet dictas mnesarchum an vis. Ut nec habeo labore invenire, sit accumsan salutandi at, et qui facer mazim persequeris.',
    images: [
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_SM-A710YZDDXTC_001_Gold_gold?$DT-Gallery$'
      },
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_SM-A710YZDDXTC_002_Gold_gold?$DT-Gallery$'
      },
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_SM-A710YZDDXTC_005_Gold_gold?$DT-Gallery$'
      }
    ]
  },
  {
    id: 8,
    name: 'Galaxy S7 edge',
    price: 34000,
    rating: 4,
    details: 'Lorem ipsum dolor sit amet, pri summo epicuri ad, suas semper epicurei eum cu. Vel illud verterem disputationi no, est omittam intellegam et. Solum blandit accommodare pri in. Eos postea labore an, amet dictas mnesarchum an vis. Ut nec habeo labore invenire, sit accumsan salutandi at, et qui facer mazim persequeris.',
    images: [
      {
        img: 'https://ss7.vzw.com/is/image/VerizonWireless/vzw-3-pack-anti-scratch-screen-protector-samsung-galaxy-s7-edge-smg9353pksp-iset?$acc-lg$'
      },
      {
        img: 'http://images.cdn.stuff.tv/sites/stuff.tv/files/styles/big-image/public/brands/Samsung/galaxy-s7-edge/_review/samsung-galaxy-s7-edge-review-verdict.jpg?itok=pdMX_Ntl'
      },
      {
        img: 'http://www.samsung.com/us/explore/galaxy-s7-features-and-specs/dist/assets/img/module6/mobile-static.jpg'
      }
    ]
  },
  {
    id: 9,
    name: '78" SUHD 4K Quantum Dot Curved Smart TV KS9000 Series 9',
    price: 120000,
    rating: 4,
    details: 'Lorem ipsum dolor sit amet, pri summo epicuri ad, suas semper epicurei eum cu. Vel illud verterem disputationi no, est omittam intellegam et. Solum blandit accommodare pri in. Eos postea labore an, amet dictas mnesarchum an vis. Ut nec habeo labore invenire, sit accumsan salutandi at, et qui facer mazim persequeris.',
    images: [
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_UA78KS9000GXXP_001_Front_black?$DT-Gallery$'
      },
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_UA78KS9000GXXP_002_Back_black?$DT-Gallery$'
      },
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_UA78KS9000GXXP_003_Side_black?$DT-Gallery$'
      }
    ]
  },
  {
    id: 10,
    name: 'NX300 (18-55 mm)',
    price: 28000,
    rating: 3,
    details: 'Lorem ipsum dolor sit amet, pri summo epicuri ad, suas semper epicurei eum cu. Vel illud verterem disputationi no, est omittam intellegam et. Solum blandit accommodare pri in. Eos postea labore an, amet dictas mnesarchum an vis. Ut nec habeo labore invenire, sit accumsan salutandi at, et qui facer mazim persequeris.',
    images: [
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_EV-NX300ZDSVPH_610_Front_black?$DT-Gallery$'
      },
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_EV-NX300ZDSVPH_615_Back_black?$DT-Gallery$'
      }
    ]
  },
  {
    id: 11,
    name: 'French Door Fridge',
    price: 45000,
    rating: 3,
    details: 'Lorem ipsum dolor sit amet, pri summo epicuri ad, suas semper epicurei eum cu. Vel illud verterem disputationi no, est omittam intellegam et. Solum blandit accommodare pri in. Eos postea labore an, amet dictas mnesarchum an vis. Ut nec habeo labore invenire, sit accumsan salutandi at, et qui facer mazim persequeris.',
    images: [
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_RF60J9070SR-TC_001_Front_silver?$DT-Gallery$'
      },
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_RF60J9070SR-TC_006_Front-Open-With-Food_silver?$DT-Gallery$'
      },
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_RF60J9070SR-TC_015_Detail4_silver?$DT-Gallery$'
      }
    ]
  },
  {
    id: 12,
    name: 'Rapid Defrost 23 L',
    price: 12000,
    rating: 4,
    details: 'Lorem ipsum dolor sit amet, pri summo epicuri ad, suas semper epicurei eum cu. Vel illud verterem disputationi no, est omittam intellegam et. Solum blandit accommodare pri in. Eos postea labore an, amet dictas mnesarchum an vis. Ut nec habeo labore invenire, sit accumsan salutandi at, et qui facer mazim persequeris.',
    images: [
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_MS23K3513AS-TC_001_Front_silver?$DT-Gallery$'
      },
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_MS23K3513AS-TC_002_Front-Open_silver?$DT-Gallery$'
      },
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_MS23K3513AS-TC_010_Low-Front-Open_silver?$DT-Gallery$'
      }
    ]
  },
  {
    id: 13,
    name: 'Top Load Fully Auto with Activ Dual Wash 8.0 kg ',
    price: 15000,
    rating: 2,
    details: 'Lorem ipsum dolor sit amet, pri summo epicuri ad, suas semper epicurei eum cu. Vel illud verterem disputationi no, est omittam intellegam et. Solum blandit accommodare pri in. Eos postea labore an, amet dictas mnesarchum an vis. Ut nec habeo labore invenire, sit accumsan salutandi at, et qui facer mazim persequeris.',
    images: [
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_WA80J5712SW-TC_001_Front_white?$DT-Gallery$'
      },
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_WA80J5712SW-TC_002_Front-Open_white?$DT-Gallery$'
      },
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_WA80J5712SW-TC_008_Detail_white?$DT-Gallery$'
      }
    ]
  },
  {
    id: 14,
    name: '1.0HP Standard Inverter Split Type Air Conditioner, 9,720 BTU/h',
    price: 18000,
    rating: 4,
    details: 'Lorem ipsum dolor sit amet, pri summo epicuri ad, suas semper epicurei eum cu. Vel illud verterem disputationi no, est omittam intellegam et. Solum blandit accommodare pri in. Eos postea labore an, amet dictas mnesarchum an vis. Ut nec habeo labore invenire, sit accumsan salutandi at, et qui facer mazim persequeris.',
    images: [
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_AR09JVFSBWKNTC_001_White_white?$DT-Gallery$'
      },
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_AR09JVFSBWKNTC_002_White_white?$DT-Gallery$'
      },
      {
        img: 'http://images.samsung.com/is/image/samsung/ph_AR09JVFSBWKNTC_008_White_white?$DT-Gallery$'
      }
    ]
  }
];
