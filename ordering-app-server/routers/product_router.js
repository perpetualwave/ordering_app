var express = require('express');
var router = express.Router();

var MockData = require('../helpers/MockData');

router.route('/')
.get(function(req, res){
  res.json(MockData);
});

module.exports = router;
