var express = require('express');
var router = express.Router();

var MockUsers = require('../helpers/MockUsers');

router.get('/', function(req, res){
    res.json({result : "working user router"});
});

router.route('/')
.post(function(req,res){
  var username = req.body.username;
  var password = req.body.password;
  var result = {
    logged_in: false
  }
  console.log(username + ":" + password);
  if(username != null && password != null){
    var user = isAuthorized(username, password)
    if(user){
      result.logged_in = true;
      result.user = user;
    }
  }
  res.json(result);
});

function isAuthorized(username, password){
  for(var x = 0 ; x < MockUsers.length; x++){
    if(MockUsers[x].username.toLowerCase() === username.toLowerCase() && MockUsers[x].password.toLowerCase() === password.toLowerCase()){
      return MockUsers[x];
    }
  }
  return null;
}

module.exports = router;
