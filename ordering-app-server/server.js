var express = require('express');
var app = express();
//var db =  monk('localhost:27017/sampledb');
var PORT = 8080;

var product_router = require('./routers/product_router');
var user_router = require('./routers/user_router');

app.use(require('body-parser').urlencoded({ extended: true }));

app.get('/', function(req, res){
    res.json({result : "connected"});
});

app.use('/products', product_router);
app.use('/login', user_router);

app.listen(PORT, function(){
  console.log('listening on port ' + PORT);
});
