'use strict'
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image
} from 'react-native';

var activeImage = require('../../images/select_star.png');
var inActiveImage = require('../../images/unselect_star.png');

class StarRating extends Component {
  constructor(props) {
    super(props);
  }

  getStar(isActive, key){
    return isActive ? (
      <Image key = {key}
             style = {{
               width: this.props.starSize,
               height: this.props.starSize,
               flex: 1
             }}
             resizeMode={Image.resizeMode.contain}
             source = {activeImage}/>
    ) : (
      <Image key = {key}
             style = {{
               width: this.props.starSize,
               height: this.props.starSize,
               flex: 1
             }}
             resizeMode={Image.resizeMode.contain}
             source = {inActiveImage}/>
    );
  }

  render(){
    var max = this.props.max;
    var val = this.props.val;
    var ratings = [];
    for(var x = 1; x <= max; x++){
      ratings.push(this.getStar(x<=val, x));
    }
    return(
      <View style = {{flexDirection: 'row', padding: 10, marginRight: this.props.styles.marginRight, marginLeft: this.props.styles.marginLeft}}>
        {ratings}
      </View>
    );
  }
}

StarRating.defaultProps = {
  max: 5,
  val: 0,
  starSize: 20,
  styles: {
    marginLeft: 0,
    marginRight: 0
  }
}

StarRating.propTypes = {
  max: React.PropTypes.number,
  val: React.PropTypes.number,
  starSize: React.PropTypes.number
}

// let styles = StyleSheet.create({
//   imgRating: {
//     width: this.props.starSize,
//     height: this.props.starSize,
//     flex: 1
//   }
// });

module.exports = StarRating;
