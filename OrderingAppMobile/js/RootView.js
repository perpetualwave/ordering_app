'use strict'
import React, { Component } from 'react';
import {
  Navigator
} from 'react-native';
var LoginView = require('./views/LoginView');
var MainView = require('./views/MainView');
class RootView extends Component{
  constructor(props){
    super(props);
  }

  renderScene(route, navigator){
    return React.createElement(route.component, { ...this.props, ...route.passProps, navigator } )
  }

  configureScene(route, routeStack){
   return {...Navigator.SceneConfigs.HorizontalSwipeJump, gestures: false}
  }

  render(){
    return (
      <Navigator
        configureScene={ this.configureScene }
        style = {{flex: 1}}
        initialRoute = {{component: LoginView}}
        renderScene = {this.renderScene.bind(this)}
        />
    );
  }
}

module.exports = RootView;
