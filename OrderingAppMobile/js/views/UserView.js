'use strict'
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableHighlight,
  Image,
  ScrollView
} from 'react-native';

class UserView extends Component {
  constructor(props) {
    super(props);
  }

  logout(){
    this.props.navigator.pop();
  }

  render(){
    return(
      <View style = {styles.container}>
        <TouchableHighlight style = {styles.btnLogout}
                            underlayColor ='#dddddd'
                            onPress = {this.logout.bind(this)}>
          <Text style = {styles.btnText}>Log Out</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

let styles = StyleSheet.create({
  container: {
    flex : 1,
    backgroundColor: '#F0EEED'
  },
  btnLogout: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'red',
    marginTop: 15,
    height: 40
  },
  btnText: {
    textAlign: 'center',
    color: 'white',
    alignSelf: 'center',
    justifyContent: 'center',
    fontSize: 18
  },
});

UserView.defaultProps = {
  user: {
    fullName: 'Sample User'
  }
}

module.exports = UserView;
