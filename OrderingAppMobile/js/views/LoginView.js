'use strict'
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableHighlight,
  Image,
  AlertIOS
} from 'react-native';

var MainView = require('./MainView');
import CheckBox from 'react-native-checkbox';
class LoginView extends Component{
  constructor(props){
    super(props);
    this.state = {
      username: '',
      password: '',
      connected: false,
      ip: 'localhost'
    };
  }

  getFormBody(){
    var details = {
      'username': this.state.username,
      'password': this.state.password
    };

    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");
    return formBody;
  }

  _login(){
    if(this.state.connected){
      var formBody = this.getFormBody();
      fetch('http://'+ this.state.ip +':8080/login/', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: formBody
      })
      .then((response) => response.json())
      .then((json) => {
        this._authenticate(json);
      })
      .catch(() => {
        alert('App may not be connected to the server');
      });
    }else{
      var user = {fullName : 'Sample User'}
      this.showMainView(user);
    }
  }

  showMainView(user){
    this.setState({
      username: '',
      password: ''
    });
    this.props.navigator.push({
      component: MainView,
      passProps: {
        connected : this.state.connected,
        ip: this.state.ip,
        user: user
      }
    });
  }

  _authenticate(res){
    if(res.logged_in){
      this.showMainView(res.user);
    }else{
      alert('Authentication Error')
    }
  }

  showConnected(checked){
    this.setState({
      connected: checked
    });
    if(checked){
      this.showIPDialog();
    }else{
      alert('App will now use mock data');
    }
  }

  showIPDialog(){
    AlertIOS.prompt('Enter server IP(e.g. 192.168.1.1)','Leave empty to use localhost',
      [
        {text: 'Okay', onPress: (ip) => this.updateIP(ip)}
      ]
    );
  }

  updateIP(ip){
    if(ip){
      this.setState({ip: ip})
    }else{
      this.setState({ip: 'localhost'})
    }
    alert('App will now use data from server, please make sure that the server is running on port 8080 and on the same network');
  }

  render(){
    return(
      <View style = {styles.container}>
        <Image style = {styles.appImage}
               source = {require('../../images/cart2.png')} />

        <TextInput style = {styles.input}
                   placeholder = "Username"
                   onChangeText = {(text) => {this.setState({username: text})}}
                   value = {this.state.username}/>

        <TextInput style = {styles.input}
                   secureTextEntry = {true}
                   placeholder = "Password"
                   onChangeText = {(text) => {this.setState({password: text})}}
                   value = {this.state.password}/>

        <TouchableHighlight style = {styles.btn}
                            underlayColor ='#dddddd'
                            onPress = {this._login.bind(this)}>
          <Text style = {styles.btnText}>Login</Text>
        </TouchableHighlight>
        <View style = {styles.checkbox}>
          <CheckBox
            label='Connect to Node.JS Server'
            labelStyle={{fontSize : 12, color: 'white'}}
            checked={this.state.connected}
            onChange={(checked) => this.showConnected(checked)}/>
        </View>
      </View>
    );
  }
}

let styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 55,
    backgroundColor: '#12186B'
  },
  appImage: {
    width: 185,
    height: 185,
    alignSelf: 'center',
    marginBottom: 25
  },
  input: {
    width: 300,
    height: 40,
    borderRadius: 8,
    padding: 4,
    margin: 5,
    borderColor: 'black',
    borderWidth: 1,
    alignSelf: 'center',
    backgroundColor: 'white'
  },
  btn:{
    alignSelf: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#FF6A00',
    borderRadius: 8,
    borderColor: 'black',
    borderWidth: 1,
    marginRight: 38,
    width: 100,
    height: 40
  },
  btnText:{
    alignSelf: 'center',
    color: 'white',
    fontSize: 24,
    marginTop: 3
  },
  checkbox: {
    alignItems: 'center'
  }
});

module.exports = LoginView;
