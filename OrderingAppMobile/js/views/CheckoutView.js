'use strict'
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableHighlight,
  Image,
  ScrollView
} from 'react-native';

var ReceiptView = require('./ReceiptView');

class CheckoutView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fullName: this.props.user.fullName,
      province: '',
      city: '',
      street: '',
      phoneNumber: '',
      ccNumber: '',
      chName: this.props.user.fullName,
      expiry: '',
      cvv: ''
    };
  }

  proceed(){
    // if(this.state.fullName == '' || this.state.province == '' || this.state.city == '' || this.state.street == '' || this.state.phoneNumber == ''
    //   || this.state.ccNumber == '' || this.state.chName == '' || this.state.expiry == '' || this.state.cvv == ''){
    //   alert('All fields are required');
    // }else{
      var info = {
        fullName: this.state.fullName,
        province: this.state.province,
        city: this.state.city,
        street: this.state.street,
        phoneNumber: this.state.phoneNumber,
        ccNumber: this.state.ccNumber,
        chName: this.state.chName,
        expiry: this.state.expiry,
        cvv: this.state.cvv
      }
      this.props.cart.clearItems();
      this.props.navigator.replace({
        component: ReceiptView,
        passProps: {
          info: info,
          items: this.props.items
        }
      });
    }
  //}

  render(){
    return(
      <ScrollView style = {styles.outerContainer}>
        <View style = {styles.innerContainer}>
          <Text style = {styles.shipToText}>Ship to:</Text>
          <TextInput style = {styles.input}
                     placeholder = "Full Name"
                     onChangeText = {(text) => {this.setState({fullName: text})}}
                     value = {this.state.fullName}/>
          <TextInput style = {styles.input}
                     placeholder = "Province"
                     onChangeText = {(text) => {this.setState({province: text})}}
                     value = {this.state.province}/>
          <TextInput style = {styles.input}
                     placeholder = "City"
                     onChangeText = {(text) => {this.setState({city: text})}}
                     value = {this.state.city}/>
          <TextInput style = {styles.input}
                     placeholder = "Street"
                     onChangeText = {(text) => {this.setState({street: text})}}
                     value = {this.state.street}/>
          <TextInput style = {styles.input}
                     placeholder = "Phone Number"
                     onChangeText = {(text) => {this.setState({phoneNumber: text})}}
                     value = {this.state.phoneNumber}/>

          <Text style = {styles.shipToText}>Credit Card Info:</Text>

          <TextInput style = {styles.input}
                     placeholder = "Credit Card Number"
                     onChangeText = {(text) => {this.setState({ccNumber: text})}}
                     value = {this.state.ccNumber}/>
          <TextInput style = {styles.input}
                     placeholder = "Cardholder's Name"
                     onChangeText = {(text) => {this.setState({chName: text})}}
                     value = {this.state.chName}/>
          <View style = {styles.textContainer}>
            <TextInput style = {styles.input}
                       placeholder = "Expiry Date"
                       onChangeText = {(text) => {this.setState({expiry: text})}}
                       value = {this.state.expiry}/>
            <TextInput style = {styles.input}
                       placeholder = "CVV"
                       onChangeText = {(text) => {this.setState({cvv: text})}}
                       value = {this.state.cvv}/>
          </View>
          <View style = {styles.buttonContainer}>
            <TouchableHighlight style = {styles.btnCancel}
                                underlayColor ='#dddddd'
                                onPress = {() => this.props.navigator.pop()}>
              <Text style = {styles.btnText}>Cancel</Text>
            </TouchableHighlight>
            <TouchableHighlight style = {styles.btnProceed}
                                underlayColor ='#dddddd'
                                onPress = {this.proceed.bind(this)}>
              <Text style = {styles.btnText}>Proceed</Text>
            </TouchableHighlight>
          </View>
        </View>
      </ScrollView>
    );
  }
}

CheckoutView.defaultProps = {
  user: {
    fullName: 'Sample User'
  },
  items: []
}

let styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    paddingTop: 25,
    backgroundColor: '#F0EEED'
  },
  innerContainer: {
    flex: 1,
    backgroundColor: 'white',
    margin: 15,
    borderWidth: 1,
    borderColor: 'black'
  },
  shipToText:{
    color: '#12186B',
    fontSize: 18,
    margin: 10
  },
  input:{
    flex: 1,
    margin: 7,
    padding: 8,
    height: 40,
    borderWidth: 1,
    borderRadius: 6,
    borderColor: 'black'
  },
  textContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  buttonContainer:{
    height: 50,
    margin: 7,
    flexDirection: 'row'
  },
  btnProceed: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FF6A00',
    flex: 7
  },
  btnCancel: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'red',
    flex: 3
  },
  btnText: {
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 20,
    justifyContent: 'center',
    color: 'white'
  }
});

module.exports = CheckoutView;
