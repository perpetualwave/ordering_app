'use strict'
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableHighlight,
  Image,
  ListView
} from 'react-native';
var sampleData = require('../helpers/MockData');
var CheckoutView = require('./CheckoutView');
class CartView extends Component{
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.trueData = [];
    this.state = {
      totalPrice: 0,
      dataSource: this.ds.cloneWithRows(this.trueData)
    }
  }

  clearItems(){
    this.trueData = [];
    this.refreshList();
  }

  addItem(item){
    var idx = this.itemExists(item);
    if(idx === -1){
      item.count = 1;
      this.trueData.push(item);
    }else{
      var toReplace = this.trueData[idx];
      toReplace.count = toReplace.count + 1;
      this.trueData[idx] = toReplace;
    }
    this.refreshList();
  }

  minusItem(item){
    var idx = this.itemExists(item);
    if(idx !== -1){
      if(item.count > 1){
        item.count--;
        this.trueData[idx] = item;
        this.refreshList();
      }
    }
  }


  removeItem(item){
    var idx = this.itemExists(item);
    if(idx !== -1){
      this.trueData.splice(idx, 1);
      this.refreshList();
    }
  }

  refreshList(){
    this.setState({
      dataSource: this.ds.cloneWithRows(this.trueData),
      totalPrice: this.countTotal()
    });
  }

  countTotal(){
    var total = 0;
    for(var x = 0; x < this.trueData.length; x++){
      total += (this.trueData[x].price * this.trueData[x].count);
    }
    return total;
  }

  itemExists(item){
    for(var x = 0; x < this.trueData.length; x++){
      if(this.trueData[x].id === item.id){
        return x;
      }
    }
    return -1;
  }

  proceedToCheckout(){
    if(this.trueData.length > 0){
      this.props.navigator.push({
        component: CheckoutView,
        passProps: {
          items: this.trueData,
          user: this.props.user,
          cart: this
        }
      });
    }else{
      alert('Add items first');
    }
  }

  renderRow(item){
    return(
      <View style = {styles.row}>
        <Image style = {styles.rowImage}
               source = {{uri: item.images[0].img}}
               resizeMode = {Image.resizeMode.contain}/>
        <View style = {styles.rowDetailView}>
          <Text style = {styles.rowItemName} numberOfLines = {1}>{item.name}</Text>
          <Text style = {styles.rowItemPrice}>₱{item.price}</Text>
          <View style = {styles.rowActionView}>
            <View style = {styles.rowPlusMinus}>
              <TouchableHighlight style = {styles.btnPlusMinus}
                                  underlayColor ='#dddddd'
                                  onPress = {this.minusItem.bind(this,item)}>
                <Text style = {styles.btnPlusMinusText}>-</Text>
              </TouchableHighlight>
              <Text style = {styles.countText}>{item.count}</Text>
              <TouchableHighlight style = {styles.btnPlusMinus}
                                  underlayColor ='#dddddd'
                                  onPress = {this.addItem.bind(this, item)}>
                <Text style = {styles.btnPlusMinusText}>+</Text>
              </TouchableHighlight>
            </View>
            <TouchableHighlight style = {styles.rowBtnDelete}
                                underlayColor = 'transparent'
                                onPress = {this.removeItem.bind(this, item)}>
              <Image style = {styles.rowBtnDeleteImage}
                     source = {require('../../images/delete.png')}/>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    );
  }

  render(){
    var show = (this.state.dataSource == null || this.trueData.length == 0) ? <Text style = {styles.noItems}> No items in your cart </Text>
               : <ListView style = {styles.listView}
                           dataSource = {this.state.dataSource}
                           renderRow = {this.renderRow.bind(this)}/>;

    return(
      <View style = {styles.container}>
        {show}
        <View style = {styles.bottomView}>
          <View style = {styles.totalView}>
            <Text style = {styles.totalLabel}>Estimated Total</Text>
            <Text style = {styles.totalPrice}>₱{this.state.totalPrice}</Text>
          </View>
          <TouchableHighlight style = {styles.btnProceed}
                              underlayColor ='#dddddd'
                              onPress = {this.proceedToCheckout.bind(this)}>
            <Text style = {styles.btnText}>Proceed To Checkout</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

CartView.defaultProps = {
  user: {
    fullName: 'Sample User'
  }
}

let styles = StyleSheet.create({
  container: {
    flex : 1,
    backgroundColor: '#F0EEED'
  },
  listView: {
    flex: 10
  },
  noItems: {
    flex : 10,
    fontSize: 20,
    textAlign: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    paddingTop: 10
  },
  bottomView: {
    flex: 2,
    borderTopWidth: 2,
    borderColor: '#737373'
  },
  totalView: {
    flexDirection: 'row',
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center'
  },
  totalLabel: {
    fontSize: 15,
    flex: 1,
    textAlign: 'left',
    marginLeft: 10
  },
  totalPrice: {
    fontSize: 15,
    flex: 1,
    textAlign: 'right',
    justifyContent: 'center',
    marginRight: 10
  },
  btnProceed: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FF6A00'
  },
  btnText: {
    textAlign: 'center',
    color: 'white',
    alignSelf: 'center',
    justifyContent: 'center',
    fontSize: 18,
  },
  row: {
    height: 130,
    margin: 3,
    backgroundColor: 'white',
    flexDirection: 'row'
  },
  rowImage: {
    height: 90,
    width: 90,
    alignSelf: 'center',
    flex: 3
  },
  rowDetailView: {
    padding: 5,
    flex: 7
  },
  rowItemName: {
    fontSize: 21,
    color: 'black',
    flex: 2
  },
  rowItemPrice: {
    fontSize: 21,
    color: 'red',
    flex: 2
  },
  rowActionView:{
    flex: 2,
    flexDirection: 'row'
  },
  rowPlusMinus: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start'
  },
  btnPlusMinus:{
    backgroundColor: '#EDEBEB',
    width: 35,
    height: 35,
    marginBottom: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnPlusMinusText:{
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 24,
    color: 'black'
  },
  countText:{
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 20,
    color: 'black',
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 5
  },
  rowBtnDelete: {
    flex: 1,
    alignItems: 'flex-end',
    marginBottom: 5
  },
  rowBtnDeleteImage:{
    width: 30,
    height: 30,
    margin: 5
  }

});

module.exports = CartView;
