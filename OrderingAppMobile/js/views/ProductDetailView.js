'use strict'
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableHighlight,
  Image,
  ScrollView
} from 'react-native';
var StarRating  = require('../components/StarRating');
var ImageSlider = require('../components/ImageSlider');
class ProductDetailView extends Component {
  constructor(props) {
    super(props);
  }

  addToCart(item){
    this.props.mainView.addItemToCart(item);
    this.props.navigator.pop();
  }

  render(){
    var ratings = <StarRating max = {5} val = {this.props.item.rating} starSize = {25}/>;
    var images = [];
    for(var x = 0; x < this.props.item.images.length; x++){
      images.push(this.props.item.images[x].img);
    }
    return(
      <View style = {styles.container}>
        <ScrollView
          style={styles.scroll}
          automaticallyAdjustContentInsets={false}
          horizontal={false}>
          <View>
            <Text style = {styles.prodName} numberOfLines = {1}> {this.props.item.name} </Text>
            <View style = {{flexDirection: 'row'}}>
              <View style = {{flex: 3}}>
                <Text style = {styles.prodPrice}> {this.props.item.price}  </Text>
                {ratings}
              </View>
              <View style = {{flex: 7}} />
            </View>

            <ImageSlider images={images} height = {250}/>
            <View style = {styles.separator}/>

            <View style = {styles.detailContainer}>
              <Text style = {styles.detailHeader}>Product Details</Text>
              <Text style = {styles.detailBody}>{this.props.item.details}</Text>
            </View>

          </View>
        </ScrollView>
        <View style = {styles.bottom}>
          <TouchableHighlight style = {styles.btnCancel}
                              underlayColor ='#dddddd'
                              onPress = {() => this.props.navigator.pop()}>
            <Text style = {styles.btnText}>Cancel</Text>
          </TouchableHighlight>
          <TouchableHighlight style = {styles.btnOkay}
                              underlayColor ='#dddddd'
                              onPress = {this.addToCart.bind(this, this.props.item)}>
            <Text style = {styles.btnText}>Add to cart</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

let styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 35
  },
  scroll: {
    flex: 9
  },
  bottom: {
    flexDirection: 'row',
    flex: 0.8
  },
  btnOkay:{
    flex: 3,
    alignItems: 'center',
    backgroundColor: '#FF6A00',
    justifyContent: 'center'
  },
  btnCancel:{
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F52C2F',
    justifyContent: 'center'
  },
  btnText:{
    alignSelf: 'center',
    color: 'white',
    fontSize: 24,
    textAlign: 'center'
  },
  prodName:{
    fontSize: 24,
    margin: 5,
    color: 'gray'
  },
  prodPrice:{
    fontSize: 18,
    marginLeft: 6,
    color: 'red'
  },
  separator: {
    height: 1,
    margin: 15,
    backgroundColor: 'gray'
  },
  detailContainer:{
    padding: 15
  },
  detailHeader: {
    fontSize: 18,
    marginBottom: 15
  },
  detailBody: {
    fontSize: 16
  }
});

module.exports = ProductDetailView;
