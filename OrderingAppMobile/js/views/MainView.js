'use strict'
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableHighlight,
  Image
} from 'react-native';
var ScrollableTabView = require('react-native-scrollable-tab-view');
var ProductListView = require('./ProductListView');
var CartView = require('./CartView');
var UserView = require('./UserView')

class MainView extends Component {
  constructor(props) {
    super(props);
  }

  addItemToCart(item){
    this.cart.addItem(item);
  }

  render(){
    return(
      <ScrollableTabView style = {styles.container}>
        <ProductListView tabLabel = "Product List" navigator = {this.props.navigator} connected = {this.props.connected} user = {this.props.user} ip = {this.props.ip} mainView = {this}/>
        <CartView tabLabel = "Cart" navigator = {this.props.navigator} ref = {(ref) => this.cart = ref} user = {this.props.user}/>
        <UserView tabLabel = "User" navigator = {this.props.navigator} user = {this.props.user}/>
      </ScrollableTabView>
    );
  }
}

let styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 25
  }
});

MainView.defaultProps = {
  connected: false,
  ip: 'localhost',
  user: {
    fullName: 'Sample User'
  }
}

module.exports = MainView;
