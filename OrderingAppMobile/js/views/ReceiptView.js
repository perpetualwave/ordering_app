'use strict'
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableHighlight,
  Image,
  ScrollView
} from 'react-native';

class ReceiptView extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount(){
    alert('Thank you for pruchasing items!');
  }

  countTotal(){
    var total = 0;
    for(var x = 0; x < this.props.items.length; x++){
      total += (this.props.items[x].price * this.props.items[x].count);
    }
    return total;
  }

  done(){
    this.props.navigator.pop();
  }

  render(){
    var items = [];
    for(var x = 0; x < this.props.items.length; x++){
      var item = this.props.items[x];
      var toAdd = (
        <View style = {styles.itemRow} key = {x}>
          <Text style = {styles.itemName} numberOfLines = {1}>{item.name}</Text>
          <Text style = {styles.itemCount} numberOfLines = {1}>{item.count}</Text>
          <Text style = {styles.itemTotal} numberOfLines = {1}>₱{(item.price * item.count)}</Text>
        </View>
      );
      items.push(toAdd);
    }

    var itemList = (
      <View style = {styles.itemList}>
        <View style = {styles.itemRowHeader} key = {x}>
          <Text style = {styles.rowHeaderTextName}>Item</Text>
          <Text style = {styles.rowHeaderTextCount}>QTY</Text>
          <Text style = {styles.rowHeaderTextTotal}>Subtotal</Text>
        </View>
        {items}
      </View>
    );

    return(
      <ScrollView style = {styles.outerContainer}>
        <View style = {styles.innerContainer}>
          <Text style = {styles.txtHeader}>Order Summary</Text>
          <View style = {styles.headerView}>
            <View style = {styles.subheaderView}>
              <Text style = {styles.subheaderTitle}>Personal Info</Text>
              <Text style = {styles.subheaderText} numberOfLines = {1}>{this.props.info.fullName}</Text>
              <Text style = {styles.subheaderText} numberOfLines = {1}>{this.props.info.phoneNumber}</Text>
            </View>
            <View style = {styles.subheaderView}>
              <Text style = {styles.subheaderTitle}>Shipping Info</Text>
              <Text style = {styles.subheaderText} numberOfLines = {1}>{this.props.info.street}</Text>
              <Text style = {styles.subheaderText} numberOfLines = {1}>{this.props.info.city}</Text>
              <Text style = {styles.subheaderText} numberOfLines = {1}>{this.props.info.province}</Text>
            </View>
          </View>

          <View style = {styles.separator} />

          {itemList}
          <View style = {styles.separator} />
          <View style = {styles.itemRow}>
            <Text style = {styles.totalPriceLabel} numberOfLines = {1}>Grand Total: </Text>
            <Text style = {styles.totalPrice} numberOfLines = {1}>₱{this.countTotal()}</Text>
          </View>
        </View>
        <TouchableHighlight style = {styles.btnDone}
                            underlayColor ='#dddddd'
                            onPress = {this.done.bind(this)}>
          <Text style = {styles.btnText}>Done</Text>
        </TouchableHighlight>
      </ScrollView>
    );
  }
}

let styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    paddingTop: 25,
    backgroundColor: '#F0EEED'
  },
  btnDone: {
    marginLeft: 15,
    marginRight: 15,
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    backgroundColor: 'green'
  },
  btnText: {
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
    alignSelf: 'center',
    justifyContent: 'center'
  },
  innerContainer: {
    flex: 1,
    backgroundColor: 'white',
    margin: 15,
    borderWidth: 1,
    borderColor: 'black'
  },
  txtHeader: {
    fontSize: 24,
    textAlign: 'center',
    fontWeight: 'bold',
    margin: 10
  },
  headerView: {
    flexDirection: 'row'
  },
  subheaderView: {
    flex: 1
  },
  subheaderTitle: {
    fontSize: 18,
    marginLeft: 5,
    fontWeight: 'bold'
  },
  subheaderText: {
    fontSize: 15,
    marginLeft: 5
  },
  separator: {
    height: 1,
    backgroundColor: 'black',
    margin: 15
  },
  itemList: {
    margin: 15
  },
  itemRowHeader:{
    flexDirection: 'row'
  },
  rowHeaderTextName:{
    flex: 2,
    fontSize: 13,
    textAlign: 'center',
    fontWeight: 'bold',
    backgroundColor: '#4DEDFF',
    color: '#0F7D8A'
  },
  rowHeaderTextCount:{
    flex: 1,
    fontSize: 13,
    textAlign: 'center',
    fontWeight: 'bold',
    backgroundColor: '#4DEDFF',
    color: '#0F7D8A'
  },
  rowHeaderTextTotal:{
    flex: 1,
    fontSize: 13,
    textAlign: 'center',
    fontWeight: 'bold',
    backgroundColor: '#4DEDFF',
    color: '#0F7D8A'
  },
  itemRow: {
    flexDirection: 'row',
    marginBottom: 5
  },
  itemName: {
    flex: 2,
    fontSize: 18
  },
  itemCount: {
    flex: 1,
    fontSize: 18,
    textAlign: 'center'
  },
  itemTotal: {
    flex: 1,
    fontSize: 18
  },
  totalPriceLabel: {
    flex: 3,
    fontSize: 18,
    margin: 15,
    textAlign: 'right'
  },
  totalPrice: {
    flex: 1.5,
    fontSize: 18,
    margin: 15,
    textAlign: 'right'
  }
});

ReceiptView.defaultProps = {
  info: {},
  items: []
}

module.exports = ReceiptView;
