'use strict'
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableHighlight,
  Image,
  ScrollView
} from 'react-native';
import GridView from 'react-native-grid-view'
var StarRating  = require('../components/StarRating');
var ProductDetailView = require('./ProductDetailView');
var data = require('../helpers/MockData');
var instance;
class ProductListView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: this.props.connected? null : data
    };
    instance = this;
  }

  componentDidMount(){
    if(this.props.connected){
      fetch('http://'+ this.props.ip +':8080/products/')
        .then((response) => response.json())
        .then((json) => {
          this._loadData(json);
        })
      .catch(() => {
        alert('App may not be connected to the server');
      });
    }
  }

  _loadData(data){
    this.setState({
      dataSource: data
    });
  }

  showItem(item){
    instance.props.navigator.push({
      component: ProductDetailView,
      passProps: {item: item, mainView: this.props.mainView}
    });
  }

  renderItem(id){
    var data = this.state.dataSource[id];
    //alert(data == null);
    if(data == null){
      return(<View style = {styles.item}/>);
    }
    var ratings = <StarRating styles = {{marginRight: 60}} max = {5} val = {data.rating} starSize = {25}/>;
    return(
      <TouchableHighlight key = {data.id}
                          style = {styles.item}
                          underlayColor ='white'
                          onPress = {() => instance.showItem(data)}>

        <View>
          <Image style = {styles.img}
                 resizeMode={Image.resizeMode.contain}
                 source = {{uri: data.images[0].img}}/>
          <Text style = {styles.prodName} numberOfLines = {1}> {data.name} </Text>
          <Text style = {styles.prodPrice}> ₱{data.price}  </Text>

        </View>
      </TouchableHighlight>
    );



  }

  render(){
    if(this.state.dataSource == null){
      return(
        <Text style = {styles.loadView}> Loading data from server </Text>
      );
    }

    var items = [];

    for(var x = 0; x< this.state.dataSource.length; x++){
      var item = this.renderItem(x);
      if(item != null){
        items.push(item);
      }
    };
    var itemsToShow = [];
    var numLen = this.state.dataSource.length;
    if(numLen % 2 != 0){
      numLen++;
    }
    var rows = parseInt(numLen/ 2);
    var ctr = 0;
    for(var x = 0; x < rows; x++){
      var rowItems = [];
      for(var y = 0; y < 2; y++){
        var idx = y + ctr;
        if(idx < this.state.dataSource.length){
            rowItems.push(items[idx]);
        }
      }

      itemsToShow.push(
        <View key = {x}
              style = {styles.row}>
          {rowItems}
        </View>
      );
      ctr += 2;
    }

    return(
      <ScrollView
              style={styles.listView}>
        <View>
        {itemsToShow}
        </View>
      </ScrollView>
    );
  }
}

ProductListView.defaultProps = {
  connected: false
};

let styles = StyleSheet.create({
  loadView:{
    flex: 1,
    textAlign: 'center',
    fontSize: 24
  },
  listView: {
    flex:1,
    backgroundColor: '#F0EEED'
  },
  item: {
    flex:1,
    margin: 2,
    width: 180,
    height: 200,
    backgroundColor: 'white'
  },
  img:{
    alignSelf: 'center',
    width: 130,
    height: 130,
    margin: 10,
    justifyContent: 'center'
  },
  prodName:{
    fontSize: 16,
    margin: 5,
    color: 'gray'
  },
  prodPrice:{
    fontSize: 12,
    marginLeft: 6,
    color: 'red'
  },
  row: {
    flexDirection:'row'
  }
});

ProductListView.defaultProps = {
  connected: false,
  ip: 'localhost',
  user: {
    fullName: 'Sample User'
  }
}

module.exports = ProductListView;
